import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;

public class AdminPanel1 {
    public static void main(String[] args) {

        String property = System.getProperty("user.dir") + "/driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);
        WebDriver driver = new ChromeDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0");
        WebElement email = driver.findElement(By.name("email"));
        email.sendKeys("webinar.test@gmail.com");
        WebElement passwd = driver.findElement(By.name("passwd"));
        passwd.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement sbmtLogin = driver.findElement(By.name("submitLogin"));
        sbmtLogin.click();


        WebDriverWait
                wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.elementToBeClickable(By.id("employee_infos")));
        driver.findElement(By.id("employee_infos")).click();
        driver.findElement(By.id("header_logout")).click();

        driver.quit();
    }
}
